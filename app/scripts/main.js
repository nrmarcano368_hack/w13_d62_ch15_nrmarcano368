angular
  .module("HackListApp",[])
  .controller("HackListController", function ($scope,$http) {

    $scope.songs = [];
    $scope.lists = [];
    $scope.playList = [];
    $scope.playListSongs = [];
    $scope.songSelected;
    $scope.listSelected;
    $scope.playListSelected = {};
    $scope.playList_name;
    $scope.playList_category;
    $scope.showPlayList = {};
    loadSongs();
    loadLists();

    //Funcion que se ejecuta al momento de cargar gulp serve, trae todas las canciones registradas en la BD
    function loadSongs () {
      $http.get('http://localhost:3000/songs').then(function(data){
        $scope.songs = data.data;      
      });       
    };

    //Funcion que se ejecuta al momento de cargar gulp serve, trae todas las listas registradas en la BD
    function loadLists () {
      $http.get('http://localhost:3000/lists').then(function(data){
        $scope.lists = data.data;      
      });       
    };

    //Funcion que es invocada cuando se selecciona una playlist en la vista
    $scope.getPlayListSelected = function() {  
      $http.get('http://localhost:3000/playlists/'+ $scope.listSelected.id).then(function(data){
        $scope.playListSelected = data.data;      
      }); 
    };

    //Funcion que elimina la cancion seleccionada de la vista
    $scope.deleteSong = function(index) {  
      $http.delete('http://localhost:3000/songs/'+ $scope.songs[index].id).then(function(response){
       loadSongs(); 
       $scope.playListSelected = {};
       $scope.listSelected = "";     
      }); 
    };

    //Funcion que elimina una cancion de la playlist que se esta editando
    $scope.deleteSongFromList = function(index) {
      $http.delete('http://localhost:3000/?list_id='+ $scope.listSelected.id +"&song_id=" + $scope.playListSelected.songs[index].id).then(function(response){
      $scope.playListSelected.songs.splice(index,1);     
      }); 
    };

    //Funcion que es invocada cuando se edita una lista y se agregan canciones nuevas a una playlist ya existente
    $scope.pushNewSong = function() {
      $scope.playListSelected.songs.push($scope.songSelected);
    };

    //Funcion que elimina una cancion de la lista, al momento que se esta creando una playlist en la lista
    $scope.deleteFromList = function(index){
      $scope.playList.splice(index, 1);
    }; 

    //Funcion que guarda los cambios enviados cuando se esta editando una playlist
    $scope.saveChanges = function() {
      $http.patch(('http://localhost:3000/playlists/'+ $scope.playListSelected.list_id),{name: $scope.playListSelected.name, category: $scope.playListSelected.category, songs: $scope.playListSelected.songs}).then(function(data){
        loadLists();
        $scope.playListSelected = {};
        $scope.songSelected = "";
      }); 
    };     

    //Funcion que crea una cancion
    $scope.createSong = function () {
      $http.post('http://localhost:3000/songs', {name: $scope.songName, artist: $scope.songArtist, genre: $scope.songGenre})
      .then(function(data){
        loadSongs();
        $scope.songName = '';
        $scope.songArtist = '';
        $scope.songGenre = '';
      }, function(response) {
        alert(response.data.errors);
      });
      
    };

    //Funcion que guarda una playlist
    $scope.savePlayList = function() {
      $http.post('http://localhost:3000/playlists', {name: $scope.showPlayList.name, category: $scope.showPlayList.category, songs: $scope.showPlayList.songs})
      .then(function(data){
        loadSongs();
        loadLists();
        $scope.playList = [];
        $scope.showPlayList = {};
        $scope.playList_name = "";
        $scope.playList_category = "";
      }, function(response) {
        alert(response.data.message);   
      });
    };

    //Funcion que arma el JSON de una playlist para luego enviarlo al Back
    $scope.createPlayList = function() {
      $scope.playList.push({
        id: $scope.songSelected.id,
        name: $scope.songSelected.name, 
        genre: $scope.songSelected.genre,
        artist: $scope.songSelected.artist
      });
    $scope.showPlayList['name'] = $scope.playList_name; 
    $scope.showPlayList['category'] = $scope.playList_category; 
    $scope.showPlayList['songs'] = $scope.playList; 
    };

  });


